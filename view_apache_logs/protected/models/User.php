<?php
/*+**********************************************************************************
* Andrew Maximov
************************************************************************************/

class User extends CActiveRecord {

  public function save($runValidation=true,$attributes=null){
    if($this->getIsNewRecord()){
      $this->token = $this->getToken();
      $password = $this->password;
      $this->password = $this->hashPassword($password);
    }
    parent::save($runValidation, $attributes);
  }

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'a5_user';
	}

  public function primaryKey(){
    return 'userid';
  }

  public function getSqlCreateTable(){
    return " CREATE TABLE IF NOT EXISTS ".$this->tableName()." (
                 ".$this->primaryKey()." INT(19) AUTO_INCREMENT,
                 username VARCHAR(255) NOT NULL,
                 password VARCHAR(255) NOT NULL,
                 first_name VARCHAR(255) NOT NULL,
                 last_name VARCHAR(255) NOT NULL,
                 token VARCHAR(255) NOT NULL,
                 PRIMARY KEY ( ".$this->primaryKey().")
            );";
	}

	public function validatePassword($password){
		return CPasswordHelper::verifyPassword($password,$this->password);
	}

	public function hashPassword($password){
		return CPasswordHelper::hashPassword($password);
	}

  public function getToken(){
    $salt = CPasswordHelper::generateSalt(5);
		return crypt(date('Y-m-d H:i:s').$this->username, $salt);
  }
}
