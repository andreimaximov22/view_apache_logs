<?php
/*+**********************************************************************************
* Andrew Maximov
************************************************************************************/

class Logs extends CActiveRecord {

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

  public function isDublicate(){
    $attributeNames = $this->attributeNames();
    $params = [];
    $attr = [];
    foreach ($attributeNames as $fieldName) {
      if($fieldName != 'logid'){
        $params[$fieldName] = $this->{$fieldName};
        $attr[] = $fieldName.'=:'.$fieldName;
      }
    }
    $log=self::model()->find(implode(' AND ', $attr), $params);
    if($log){
      return true;
    } else {
      return false;
    }
  }

	public function tableName(){
    return 'a5_logs_apache_access';
		//return '{{logs_apache_access}}';
	}

  public function primaryKey(){
    return 'logid';
  }

  public function getSqlCreateTable(){
    return " CREATE TABLE IF NOT EXISTS ".$this->tableName()." (
               ".$this->primaryKey()." INT(19) AUTO_INCREMENT,
               ip VARCHAR(255) NOT NULL,
               identity VARCHAR(19) NOT NULL,
               user_id VARCHAR(255) NOT NULL,
               date_time datetime NOT NULL,
               timezone VARCHAR(255) NOT NULL,
               metod VARCHAR(255) NOT NULL,
               path VARCHAR(255) NOT NULL,
               protocol VARCHAR(19) NOT NULL,
               status INT(9) NOT NULL,
               size VARCHAR(19) NOT NULL,
               referer VARCHAR(255) NOT NULL,
               agent VARCHAR(255) NOT NULL,
               PRIMARY KEY (".$this->primaryKey().")
            );";
	}

}
