<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

class UninstallCommand extends A5CliParent {

  public $fields = [];
  public $countFieldsRequired = 0;
  public $rawData = [];

  public function run($args){
    $this->uninstall();
    echo 'Таблицы приложение удалены из базы данных...' .PHP_EOL .PHP_EOL;
  }


  public function uninstall(){
    $db = $this->db();
    foreach ($this->getSqlComands() as $sql) {
      $db->createCommand($sql)->execute();
    }
  }

  public function getSqlComands(){
    $result = array();
    foreach ($this->modules as $moduleName) {
      $model = $moduleName::model();
      $tableName = $model->tableName();
      if($this->checkExistTable($tableName)){
        $result[] = " DROP TABLE ".$tableName." ";
      }
    }
    return $result;
  }

  public function getHelp(){
    return <<<EOD
USAGE
  ./yiic uninstall

DESCRIPTION
  Эта команда удаляет все таблицы в базе данных.
  Для этой команды параметры не нужны

EXAMPLE
 * ./yiic uninstall
   Будут удалены таблицы данного приложения.


EOD;
  }

}
