<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

class UseraddCommand extends A5CliParent {

  public $fields = ['username', 'password', 'first_name', 'last_name'];
  public $countFieldsRequired = 2;
  public $minCountSimbolPass = 6;
  public $rawData = [];

  public function run($args){
    $this->validationRequest($args);
    $this->setModelDataFromRequest($args);
    $this->saveRecord();
    echo 'Пользоваетль создан...' .PHP_EOL .PHP_EOL;
  }


  public function validationRequest($args){
    parent::validationRequest($args);
    if(iconv_strlen($args[1]) < $this->minCountSimbolPass){
      echo 'Пароль должен быть не менее 6 символов...' .PHP_EOL .PHP_EOL;
      exit;
    }
  }


  public function saveRecord(){
    $userModel = User::model();
    foreach ($this->getData() as $key => $value) {
      $userModel->{$key} = $value;
    }
    $userModel->setIsNewRecord(1);
    $userModel->save(false);
  }


  public function getHelp(){
    return <<<EOD
USAGE
  ./yiic useradd [parameter]

DESCRIPTION
  Эта команда создает нового пользователя.
  Для создания используте параметры соответсвенно очередности:
  * user (обязательный)
  * password (обязательный, не менее 6 символов)
  * Имя (необязательный)
  * Фамилия (необязательный)

EXAMPLE
 * ./yiic useradd  test 123456 Андрей Максимов
   Будет создан новый пользователь с такими параметрами.


EOD;
  }

}
