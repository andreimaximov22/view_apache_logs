<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

class RunparserCommand extends A5CliParent {

  private $patternParse = "/(\S+) (\S+) (\S+) \[([^:]+):(\d+:\d+:\d+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) (\".*?\") (\".*?\")/";
  private $attributeNames = [];
  private $recordModel = false;

  public function run($args){
    $this->validationRequest($args);
    $this->recordModel = Logs::model();
    $this->attributeNames = $this->recordModel->attributeNames();
    $this->process();
    echo 'Текущая информация перенесена в базу данных...' .PHP_EOL .PHP_EOL;
  }


  public function process(){
    $files = $this->getFiles();
    foreach ($files as $filePath) {
      if(file_exists($filePath)){
        $_fp = fopen($filePath, "r");
        while (($buffer = fgets($_fp, 4096)) !== false) {
          preg_match($this->patternParse, $buffer, $result);
          $this->saveRecord($result);
        }
      }
    }
  }


  public function saveRecord($result){
    if(count($result)){
      $recordModel = Logs::model();
      $i = 0;
      foreach ($result as $key => $value) {
        $i++;
        if($key == 5 || $key == 0) {
          $i--;
          continue;
        }
        $fieldName = $this->attributeNames[$i];
        if($key == 4){
          $recordModel->{$fieldName} = date("Y-m-d", strtotime($this->formatDate($value))).' '.$result[5];
        } else {
          $recordModel->{$fieldName} = $value;
        }
      }
      $recordModel->setIsNewRecord(1);
      $recordModel->logid = '';
      if(!$recordModel->isDublicate()){
        $recordModel->save(false);
      }
      unset($recordModel);
    }
  }

  public function getFiles(){
    $result = [];
    $dir = Yii::app()->params['pathLogsApache'];
    $maskFileName = Yii::app()->params['maskFileName'];
    if(!$dir){
      echo 'Установите дирикторию для получния файлов в конфигурационном файле view_apache_logs/protected/config.php ...' .PHP_EOL .PHP_EOL;
      exit;
    }
    $masksFileName = Yii::app()->params['masksFileName'];
    if(!$masksFileName || count($masksFileName) == 0){
      echo 'Установите хотя бы одну маску файла в конфигурационном файле view_apache_logs/protected/config.php ...' .PHP_EOL .PHP_EOL;
      exit;
    }
    if(is_dir($dir)) {
      foreach ($masksFileName as $maskFileName) {
        foreach (glob("$dir/$maskFileName") as $file) {
          $result[] = $file;
        }
      }
    } else {
      echo $dir.' такой директории нет...' .PHP_EOL .PHP_EOL;
      exit;
    }
    return array_unique($result);
  }

  public function formatDate($strDate){
    $strDateArr = explode('/', $strDate);
    $strDateArr[1] = $this->getMonthNumberFromStr($strDateArr[1]);
    return implode('-', $strDateArr);
  }

  public function getMonthNumberFromStr($strMonth){
    $arrMonth = array('Jan' => 1,'Feb' => 2,'Mar' => 3,'Apr' => 4,'May' => 5,'June' => 6,
                      'July' => 7,'Aug' => 8,'Sept' => 9,'Oct' => 10,'Nov' => 11,'Dec' => 12);
    return $arrMonth[$strMonth];
  }


  public function getHelp(){
    return <<<EOD
USAGE
  ./yiic runparser

DESCRIPTION
  Эта команда запускает перенос информации из файлов apache2/access.log в базу данных.
  Для этой команды параметры не нужны

EXAMPLE
 * ./yiic runparser
   Данные будут перенесены.


EOD;
  }

}
