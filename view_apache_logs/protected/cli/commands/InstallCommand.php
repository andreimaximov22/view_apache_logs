<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

class InstallCommand extends A5CliParent {

  public $fields = [];
  public $countFieldsRequired = 0;
  public $rawData = [];

  public function run($args){
    $this->install();
    echo 'Необходимые таблицы созданы, теперь можете создать пользователя...  Смотри: ./yiic help useradd ' .PHP_EOL .PHP_EOL;
  }


  public function install(){
    $db = $this->db();
    foreach ($this->modules as $moduleName) {
      $model = $moduleName::model();
      $sql = $model->getSqlCreateTable();    
      $db->createCommand($sql)->execute();
    }
  }


  public function getHelp(){
    return <<<EOD
USAGE
  ./yiic install

DESCRIPTION
  Эта команда создает необходимые таблицы в базе данных для работы приложения.
  До это Вам необходимо создать базу данных и указать параметры подключения к ней в файле  view_apache_logs/protected/config.php
  Для этой команды параметры не нужны

EXAMPLE
 * ./yiic install
   Будут созданы необходимые таблицы.


EOD;
  }

}
