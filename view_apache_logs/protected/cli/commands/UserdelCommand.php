<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

class UserdelCommand extends A5CliParent {

  public $fields = ['username'];
  public $countFieldsRequired = 1;
  public $rawData = [];

  public function run($args){
    $this->validationRequest($args);
    $this->setModelDataFromRequest($args);
    $this->deleteRecord();
    echo 'Пользоваетль удален...' .PHP_EOL .PHP_EOL;
  }


  public function deleteRecord(){
    $userModel=User::model()->find('username=:username', array(':username'=>$this->get('username')));
    if($userModel){
      $userModel->delete();
    } else {
      echo 'Такого пользователя нет...' .PHP_EOL .PHP_EOL;
      exit;
    }
  }


  public function getHelp(){
    return <<<EOD
USAGE
  ./yiic userdel [parameter]

DESCRIPTION
  Эта команда удаляет пользователя по его user, который используется для входа в приложение.
  Для удаления используте параметры соответсвенно очередности:
  * user (обязательный)

EXAMPLE
 * ./yiic userdel  test
   Будет удален данный пользователь.


EOD;
  }

}
