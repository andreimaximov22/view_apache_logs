<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

class A5CliParent extends CConsoleCommand {

  public $fields = [];
  public $countFieldsRequired = 0;
  public $rawData = [];
  public $modules = ['User', 'Logs'];
  
  public function validationRequest($args){
    $this->checkInstall();
    if(count($args) < $this->countFieldsRequired){
      echo 'Укажите все обязательные параметры...  Cмотри: ./yiic help <command-name>' .PHP_EOL .PHP_EOL;
      exit;
    }
  }

  public function checkInstall(){
    $isInstall = 1;
    foreach ($this->modules as $moduleName) {
      $model = $moduleName::model();
      $tableName = $model->tableName();
      if(!$this->checkExistTable($tableName)){
        $isInstall = 0;
      }
    }
    if(!$isInstall){
      echo 'Приложение не установлено...  Запустите команду: ./yiic install' .PHP_EOL .PHP_EOL;
      exit;
    }
  }

  public function checkExistTable($tableName){
    $db = $this->db();
    $sql = " SELECT * FROM information_schema.tables WHERE table_name = '".$tableName."'  LIMIT 1 ";
    $result = $db->createCommand($sql)->queryRow();
    return $result;
  }

  public function setModelDataFromRequest($args){
    foreach ($args as $key => $value) {
      $this->set($this->fields[$key], $value);
    }
  }

  public function setData($data){
    $this->rawData = $data;
  }
  public function getData(){
    return $this->rawData;
  }

  public function set($fieldName, $value){
    $this->rawData[$fieldName] = $value;
  }

  public function get($fieldName){
    return $this->rawData[$fieldName];
  }

  public function db() {
    return Yii::app()->db;
  }

}
