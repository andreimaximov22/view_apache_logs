
<?php
/**
 * @package  api-client-php
 * @author   Apps5 <info@apps5.ru>
 * @link     http://apps5.ru
 */

class A5Log {
  const LOG_FILE = 'a5.log';

  static function log($message, $title = false, $file = false){
    if($title){
       $title = print_r($title, true).'\n';
       }
    $log = $title . date('Y-m-d H:i:s') . ' ' . print_r($message, true);
    if(!$file){
       $file = __DIR__ .'/'.self::LOG_FILE;
       }
	  //file_put_contents(__DIR__ . '/../../'.$file, $log . PHP_EOL, FILE_APPEND);
    file_put_contents($file, $log . PHP_EOL, FILE_APPEND);
  }

}
