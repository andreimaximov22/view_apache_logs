<?php
/*+**********************************************************************************
* Andrew Maximov
************************************************************************************/

class A5Controller extends CController {

  protected $errorMessage = false;

  protected function getLogin(){
    $isLogin = false;
    $token = $this->getBearerToken();
    if($token){
      $user = User::model()->find('token = ?', array($token));
      if($user){
        global $current_user;
        $current_user = $user;
        $isLogin = true;
      }
    }
    if(!$isLogin){
      $this->errorMessage = 'Error Authenticate';
      header('HTTP/1.0 401 Unauthorized');
      $result = array('error' => array('message' => $this->errorMessage));
      $this->responseJson($result);
      exit;
    }
  }

  protected function getRequestData(){
    return json_decode(Yii::app()->request->rawBody, true);
  }

  protected function getAuthorizationHeader(){
    $headers = null;
    if (isset($_SERVER['Authorization'])) {
        $headers = trim($_SERVER["Authorization"]);
    }
    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    } elseif (function_exists('apache_request_headers')) {
        $requestHeaders = apache_request_headers();
        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        if (isset($requestHeaders['Authorization'])) {
            $headers = trim($requestHeaders['Authorization']);
        }
    }
    return $headers;
  }

  protected function getBearerToken() {
    $headers = $this->getAuthorizationHeader();
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return null;
}

  protected function responseJson($data){
    $this->layout = false;
    ob_clean();
    header('Content-type: application/json; charset=UTF-8');
    echo CJavaScript::jsonEncode($data);
    Yii::app()->end();
  }

}
