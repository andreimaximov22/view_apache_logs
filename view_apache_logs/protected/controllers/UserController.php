<?php
/*+**********************************************************************************
* Andrew Maximov
************************************************************************************/

class UserController extends A5Controller{

  const ERROR_USERNAME_INVALID = 'Нет такого пользователя';
  const ERROR_PASSWORD_INVALID = 'Неверный пароль';

	public function actionLogin(){
		$db = Yii::app()->db;

		$requestData = $this->getRequestData();
		$username = trim($requestData['username']);
		$password = $requestData['password'];

		$user = User::model()->find('LOWER(username) = ?', array(strtolower($username)));
		if($user === null){
			$this->errorMessage = self::ERROR_USERNAME_INVALID;
		} else if(!$user->validatePassword($password)){
			$this->errorMessage = self::ERROR_PASSWORD_INVALID;
		} else {
			$token = $user->token;
		}

		if($this->errorMessage){
			$result = array('error' => array('message' => $this->errorMessage));
		} else {
			$result = array('result' => array('user' => array('token' => $token, 'firstName' => $user->first_name, 'lastName' => $user->last_name)));
		}

    $this->responseJson($result);
	}

}
