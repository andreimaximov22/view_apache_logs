<?php
/*+**********************************************************************************
* Andrew Maximov
************************************************************************************/

class LogsController extends A5Controller{

  private $limit = 100;
  public $errorMessage = false;

	public function actionGetLogs(){

    $this->getLogin();
		$db = Yii::app()->db;
		$requestData = $this->getRequestData();

    $sql = $this->getQuerysFromRequest($requestData);
    $records = $db->createCommand($sql['getCountRecords'])->queryAll();
    $countPage = round(count($records)/$this->limit);
    $records = $db->createCommand($sql['getRecords'])->queryAll();

		if($this->errorMessage){
			$result = array('error' => array('message' => $this->errorMessage));
		} else {
			$result = array('result' => array('logs' => $records, 'countPage' => $countPage));
		}

    $this->responseJson($result);

	}

  public function getQuerysFromRequest($requestData){
    $offset = 0;
    $startDate = false;
    $endDate = false;
    $ip = false;

    if(isset($requestData['ip']) && !empty($requestData['ip'])){
      $ip = $requestData['ip'];
    }
    if(isset($requestData['startDate']) && !empty($requestData['startDate'])){
      $startDate = date("Y-m-d", strtotime($requestData['startDate'])).' 00-00-00';
    }
    if(!$startDate) {
      throw new CHttpException(400,'Not set startDate report.');
    }
    if(isset($requestData['endDate']) && !empty($requestData['endDate'])){
      $endDate = date("Y-m-d", strtotime($requestData['endDate'])).' 23-59-59';
    }
    if(!$endDate) {
      throw new CHttpException(400,'Not set endDate report.');
    }
    if(isset($requestData['ctx']) && !empty($requestData['ctx'])){
      $ctx = $requestData['ctx'];
    }
    if(isset($requestData['currentPage']) && !empty($requestData['currentPage'])){
      (int)$offset = (int)($requestData['currentPage']-1)*$this->limit;
    }
    $sortBy = 'date_time';
    $sort = 'DESC';
    if(isset($ctx['sortBy']) && !empty($ctx['sortBy'])){
      $sortBy = $ctx['sortBy'];
      if(isset($ctx['sortDesc'])){
        if($ctx['sortDesc']){
          $sort = 'DESC';
        } else {
          $sort = 'ASC';
        }
      }
    }
    $logsModel = Logs::model();
    $tableName = $logsModel->getDbConnection()->getSchema()->getTable($logsModel->tableName());

    $where = '';
    if($ip){
      $where = " AND ip LIKE '%".$ip."%' ";
    }

    $sql['getCountRecords'] = " SELECT * FROM ".$tableName->name." WHERE date_time >= '".$startDate."' AND date_time <= '".$endDate."' ".$where."  ORDER BY ".$sortBy." ".$sort." ";
    $sql['getRecords'] = $sql['getCountRecords']." LIMIT ".$this->limit." OFFSET ".$offset." ";
    return $sql;
  }

}
