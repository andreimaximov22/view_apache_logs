<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

require_once(dirname(__FILE__).'/A5/A5Log.php');
require_once(dirname(__FILE__).'/../../framework/yii.php');

$config = dirname(__FILE__).'/config.php';
require_once(dirname(__FILE__).'/cli/A5CliParent.php');

defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));
defined('YII_DEBUG') or define('YII_DEBUG',true);

if(isset($config)) {
  if(is_file($config)) $configArray = include $config;
  $configArray['basePath'] = dirname(__FILE__).DIRECTORY_SEPARATOR.'..';
  $configArray['import'] = array('application.protected.models.*',
                                 );
	$app=Yii::createConsoleApplication($configArray);
	$app->commandRunner->addCommands(dirname(__FILE__).'/cli/commands');
}
else
	$app=Yii::createConsoleApplication(array('basePath'=>dirname(__FILE__).'/protected/cli'));

$env=@getenv('YII_CONSOLE_COMMANDS');
if(!empty($env))
	$app->commandRunner->addCommands($env);

$app->run();
