<?php
/***********************************************************************************
 * Andrew Maximov
 ************************************************************************************/

return
array(
    'name'=>'Vue + Yii',
    'components'=>array(
        'db'=>array(
            'class'=>'CDbConnection',
//Нужно установить
            'connectionString'=>'mysql:host=localhost;dbname=view_apache_logs',
            'username'=>'view_apache_logs',
            'password'=>'1111111111',
//////////////////              
            'emulatePrepare'=>true,
            'tablePrefix' => 'a5_',
        ),
    ),
    'import'=>array(
        'application.models.*',
    ),
    'params'=>[
//Нужно установить
      'pathLogsApache'=>'/var/log/apache2',
      'masksFileName'=>['access.log.[0-9]', 'access.log']
//////////////////
]
);
