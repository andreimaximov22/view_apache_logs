import config from 'config';
import { authHeader } from '../_helpers';

export const userService = {
    login,
    logout,
    register,
    getAll,
    getAllLogs,
    getById,
    update,
    delete: _delete
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`${config.apiUrl}?r=user/login`, requestOptions)
        .then(handleResponse)
        .then(user => {
            if (user.user.token) {
                localStorage.setItem('user', JSON.stringify(user.user));
            }
            return user;
        });
}

function logout() {
    localStorage.removeItem('user');
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}r=user/register`, requestOptions).then(handleResponse);
}

function getAll() {
    const requestOptions = {
        method: 'POST',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}?r=user/getusers`, requestOptions).then(handleResponse);
}

function getAllLogs(params) {
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(params)
    };

    return fetch(`${config.apiUrl}?r=logs/getlogs`, requestOptions).then(handleResponse);
}


function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}?r=user/getuser&userid=${id}`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}?r=user/update&userid=${user.id}`, requestOptions).then(handleResponse);
}

function _delete(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}?r=user/delete&userid=${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    //return response.text().then(text => {
    return response.json().then(text => {
        //const data = text && JSON.parse(text);
        const data = text;
        if(data.error){
          if(response.status === 401){
            logout();
            location.reload(true);
          } else {
            const error = (data && data.error.message);
            return Promise.reject(error);
          }
        }
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        //return data;
        return data.result;
    });
}
