<?php
/*+**********************************************************************************
* Andrew Maximov
************************************************************************************/

require_once(dirname(__FILE__).'/protected/A5/A5Log.php');
require_once(dirname(__FILE__).'/../framework/yii.php');
require_once(dirname(__FILE__).'/protected/controllers/A5Controller.php');

$configFile = 'protected/config.php';
$app = Yii::createWebApplication($configFile);
$app->run();
